#include <jni.h>
#include <android/log.h>

#include <GLES3/gl31.h>
#include <GLES/gl.h>

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <string>
#include <vector>
#include <iostream>

#include "utils/Point.h"
#include "../../../libs/glm/gtc/matrix_transform.hpp"

#define  LOG_TAG    "native-lib-new"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

using namespace std;

int screen_width;
int screen_height;

//glm::mat4 Projection;
//glm::mat4 View;
//glm::mat4 Model;
//glm::mat4 mvp;
//GLuint MatrixID;

vector<Point> points; //touch points vector(before Bezier points)
vector<GLfloat> touchPoints; //Bezier points vector
vector<GLint> touchIndices; //Bezier points index vector
vector<GLfloat> pointColors; //Bezier points color vector
vector<GLfloat> colorVector{0.9, 0.1, 0.1, 1.0}; //initial stroke color

GLuint shaderProgram;
GLuint vPosition; //vertex position
GLuint vColor; //vertex color
GLuint fbo; //frame buffer
GLuint vao; //vertex array object
GLuint renderedTexture; //render target texture

int numPoints = 0; //number of Bezier points
int index = 0; //index of Bezier point
int step = -2; //for Bezier method 1
int counter = 0; //for Bezier method 2
int turn = 1; //for pick colors for strokes
int whichTexture = 0; //0 for whiteTexture, 1 for coloredTexture
bool needRenderTarget = true;

// for render target algo
const GLfloat vertices[3][8] = {{0.2, 0.5, 0.2, 0.0, 0.0, 0.0, 0.0, 0.5},
                                {0.5, 0.5, 0.5, 0.0, 0.3, 0.0, 0.3, 0.5},
                                {0.5, 0.5, 0.5, 0.0, 0.0, 0.0, 0.0, 0.5}};

const GLfloat texCords[3][8] = {{1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0},
                                {1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0},
                                {1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0}};

//const GLfloat colors[2][16] = {{1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0},
//                               {0.0, 1.0, 0.0, 0.6, 0.0, 1.0, 0.0, 0.6, 0.0, 1.0, 0.0, 0.6, 0.0, 1.0, 0.0, 0.6}};

static const char glVertexShader[] =
        "#version 310 es\n"
        //"uniform mat4 uMVPMatrix;\n"
        "in vec4 vPosition;\n"
        "in vec4 vColor;\n"
        "out vec4 vertexColor;\n"
        "in vec2 vertexTextureCord;\n"
        "out vec2 textureCord;\n"
        //"uniform mat4 projection;\n"
        //"uniform mat4 modelView;\n"
        "void main()\n"
        "{\n"
        "   gl_PointSize = 6.0;\n"                                         //uncomment for touch points algo
        "   gl_Position = vPosition;\n"
        "   vertexColor = vColor;\n"                                        //uncomment for touch points algo
        //"   gl_Position = uMVPMatrix * vPosition;\n"
        //"   gl_Position = projection * modelView * vertexPosition;\n"
        //"   textureCord = vertexTextureCord;\n"                           //uncomment for render targets algo
        "}\n";

static const char glFragmentShader[] =
        "#version 310 es\n"
        "precision mediump float;\n"
        "uniform sampler2D tex;\n"
        "in vec2 textureCord;\n"
        "in vec4 vertexColor;\n"
        "out vec4 fragColor;\n"
        "void main()\n"
        "{\n"
        //"   fragColor = vec4(0.8, 0.0, 0.8, 1.0);\n"
        "   fragColor = texture(tex, gl_PointCoord) * vertexColor;\n"       //uncomment for touch points algo
        //"   fragColor = texture(tex, textureCord);\n"                     //uncomment for render targets algo
        //"   fragColor = vertexColor;\n"
        "}\n";

void init(){ //onSurfaceCreated
    // Get a handle for our "MVP" uniform
    // Only during the initialisation
    //MatrixID = glGetUniformLocation(shaderProgram, "uMVPMatrix");

    // Camera matrix
    //View = glm::lookAt(
    //        glm::vec3(10.0f,10.0f,10.0f), // Camera is at (4,3,3), in World Space
    //        glm::vec3(0.0f,0.0f,0.0f), // and looks at the origin
    //        glm::vec3(0.0f,1.0f,0.0f)  // Head is up (set to 0,-1,0 to look upside-down)
    //);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void resize(int width, int height){ //onSurfaceChanged
    if (height == 0){
        height = 1;
    }
    glViewport(0, 0, width, height);

    screen_width = width;
    screen_height = height;
    __android_log_print(ANDROID_LOG_INFO, "tag", "Screen height = %d Screen width = %d", width, height);

    // Projection matrix : 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
    //Projection = glm::perspective(glm::radians(45.0f), (float) width / (float)height, 0.1f, 100.0f);
    // Model matrix : an identity matrix (model will be at the origin)
    //Model = glm::mat4(1.0f);
}

GLuint loadShader(GLenum shaderType, const char* shaderSource)
{
    GLuint shader = glCreateShader(shaderType);
    if (shader)
    {
        glShaderSource(shader, 1, &shaderSource, nullptr);
        glCompileShader(shader);
        GLint compiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
        if (!compiled)
        {
            GLint infoLen = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
            if (infoLen)
            {
                char * buf = (char*) malloc(infoLen);
                if (buf)
                {
                    glGetShaderInfoLog(shader, infoLen, nullptr, buf);
                    LOGE("Could not Compile Shader %d:\n%s\n", shaderType, buf);
                    free(buf);
                }
                glDeleteShader(shader);
                shader = 0;
            }
        }
    }
    return shader;
}

GLubyte whiteTexture[9 * 4] =
    {
        235, 235, 235, 255,
        235, 235, 235, 255,
        235, 235, 235, 255,
        235, 235, 235, 255,
        235, 235, 235, 255,
        235, 235, 235, 255,
        235, 235, 235, 255,
        235, 235, 235, 255,
        235, 235, 235, 255
    };

GLubyte coloredTexture[9 * 4] =
    {
        235, 5, 235, 255,
        235, 5, 235, 255,
        235, 5, 235, 255,
        235, 5, 235, 255,
        235, 5, 235, 255,
        235, 5, 235, 255,
        235, 5, 235, 255,
        235, 5, 235, 255,
        235, 5, 235, 255
    };

void loadSimpleTexture(int textureId){
    //Texture Object Handle
    GLuint texture;
    //Use tightly packed data
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    //Generate a texture object
    glGenTextures(1, &texture);
    //Activate a texture
    glActiveTexture(GL_TEXTURE0);
    //Bind the texture object
    glBindTexture(GL_TEXTURE_2D, texture);

    if (textureId == 0) {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 3, 3, 0, GL_RGBA, GL_UNSIGNED_BYTE, whiteTexture);
    } else if(textureId == 1){
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 3, 3, 0, GL_RGBA, GL_UNSIGNED_BYTE, coloredTexture);
    } else{
        //3 x 3 Image,  R G B A Channels RAW Format
        GLubyte pixels[9 * 4] =
            {
                18,  140, 171, 255, //Some Colour Bottom Left
                143, 143, 143, 255, //Some Colour Bottom Middle
                255, 255, 255, 255, //Some Colour Bottom Right
                255, 255, 0,   255, //Yellow Middle Left
                0,   255, 255, 255, //Some Colour Middle
                255, 0,   255, 255, //Some Colour Middle Right
                255, 0,   0,   255, //Red Top Left
                0,   255, 0,   255, //Green Top Middle
                0,   0,   255, 255  //Blue Top Right
            };
        //Load the texture
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 3, 3, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    }
    glGenerateMipmap(GL_TEXTURE_2D);
    //Set the filtering mode
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glEnable(GL_TEXTURE_2D);
}

GLuint createProgram(const char* vertexSource, const char * fragmentSource)
{
    GLuint vertexShader = loadShader(GL_VERTEX_SHADER, vertexSource);
    if (!vertexShader){
        return 0;
    }
    GLuint fragmentShader = loadShader(GL_FRAGMENT_SHADER, fragmentSource);
    if (!fragmentShader){
        return 0;
    }
    GLuint program = glCreateProgram();

    if (program){
        glAttachShader(program , vertexShader);
        glAttachShader(program, fragmentShader);
        glLinkProgram(program);
        GLint linkStatus = GL_FALSE;
        glGetProgramiv(program , GL_LINK_STATUS, &linkStatus);
        if (linkStatus != GL_TRUE){
            GLint bufLength = 0;
            glGetProgramiv(program, GL_INFO_LOG_LENGTH, &bufLength);
            if (bufLength){
                char* buf = (char*) malloc(bufLength);
                if (buf){
                    glGetProgramInfoLog(program, bufLength, nullptr, buf);
                    LOGE("Could not link program:\n%s\n", buf);
                    free(buf);
                }
            }
            glDeleteProgram(program);
            program = 0;
        }
    }
    return program;
}

// control points
float p0x, p0y, p1x, p1y, p2x, p2y;

// find t value of middle touch point
float findMidpointT(Point p0, Point p1, Point p2){
    auto ac = sqrt(pow(p0.x-p2.x, 2) + pow(p0.y-p2.y, 2));
    auto ab = sqrt(pow(p0.x-p1.x, 2) + pow(p0.y-p1.y, 2));
    auto theta_ac = atan2(abs(p0.y-p2.y), abs(p0.x-p2.x));
    auto theta_ab = atan2(abs(p0.y-p1.y), abs(p0.x-p1.x));
    auto ab_costheta = ab * cos(abs(theta_ab-theta_ac));
    auto t = ab_costheta/ac;
    return t;
}

// set 3 control points
void setControlPoints(Point p0, Point p1, Point p2, float t) {
    p0x = p0.x;
    p0y = p0.y;
    p2x = p2.x;
    p2y = p2.y;
    p1x = (p1.x - (1.0f - t) * (1.0f - t) * p0x - t * t * p2x) / (2.0f * t * (1.0f - t));
    p1y = (p1.y - (1.0f - t) * (1.0f - t) * p0y - t * t * p2y) / (2.0f * t * (1.0f - t));
}

// find x coordinate
float bezierFunctionX(float t){
    float xcoord = (1.0f-t)*(1.0f-t)*p0x + 2*(1.0f-t)*t*p1x + t*t*p2x;
    //float xcoord = (1.0f-t)*(1.0f-t)*(1.0f-t)*p0x + 3.0f*(1.0f-t)*(1.0f-t)*t*p1x + 3.0f*(1.0f-t)*t*t*p2x + t*t*t*p3x;
    float xCord =  xcoord * 2.0f / float(screen_width) - 1.0f;
    return xCord;
}

// find y coordinate
float bezierFunctionY(float t){
    float ycoord = (1.0f-t)*(1.0f-t)*p0y + 2*(1.0f-t)*t*p1y + t*t*p2y;
    //float ycoord = (1.0f-t)*(1.0f-t)*(1.0f-t)*p0y + 3.0f*(1.0f-t)*(1.0f-t)*t*p1y + 3.0f*(1.0f-t)*t*t*p2y + t*t*t*p3y;
    float yCord =  ycoord * -2.0f / float(screen_height) + 1.0f;
    return yCord;
}

// Bezier - Method 1
void bezierGeneration1(int stepValue){
    Point begin;
    Point control;
    Point end;
    int n = points.size();

    if (stepValue == 0){
        begin = points[n-2];
        end = Point::middle(points[n-2], points[n-1]);
        control = Point::middle(begin, end);
    } else{
        begin = Point::middle(points[n-3], points[n-2]);
        control = points[n-2];
        end = Point::middle(points[n-2], points[n-1]);
    }

    float distance = begin.distance(end);
    int segments = max(int(distance/3), 10);
    for (int i = 1; i < segments+1; i++){
        auto t = i / double(segments);
        auto xcoord = pow(1-t, 2) * begin.x + 2.0 * (1-t) * t * control.x + t * t * end.x;
        touchPoints.emplace_back(xcoord * 2.0f / screen_width - 1.0f);
        auto ycoord = pow(1-t, 2) * begin.y + 2.0 * (1-t) * t * control.y + t * t * end.y;
        touchPoints.emplace_back(ycoord * -2.0f / screen_height + 1.0f);
        touchIndices.emplace_back(index);
        pointColors.insert(pointColors.end(), colorVector.begin(), colorVector.end());
        numPoints = index + 1;
        index += 1;
    }
}

/* Bezier - Method 2
void bezierGeneration2(int counterValue){
    int n = points.size();
    if (counterValue == 1){
        Point midPoint = Point::middle(points[n-2], points[n-1]);
        float mid_t = findMidpointT(points[n-2], midPoint, points[n-1]);
        setControlPoints(points[n-2], midPoint, points[n-1], mid_t);
        float distance = points[n-2].distance(points[n-1]);
        int segments = int(distance / 3);
        for (int i = 0; i < segments+1; i++) {
            auto t = i / double(segments);
            touchPoints.emplace_back(bezierFunctionX(t));
            touchPoints.emplace_back(bezierFunctionY(t));
            touchIndices.emplace_back(index);
            pointColors.insert(pointColors.end(), colorVector.begin(), colorVector.end());
            numPoints = index + 1;
            index += 1;
        }

    } else{
        float mid_t = findMidpointT(points[n - 3], points[n - 2], points[n - 1]);
        setControlPoints(points[n - 3], points[n - 2], points[n - 1], mid_t);
        float distance1 = points[n - 3].distance(points[n - 1]);
        int segments1 = int(distance1 / 3);
        float distance2 = points[n - 3].distance(points[n - 2]);
        int segments2 = int(distance2 / 3);
        for (int i = segments2+1; i < segments1+1; i++) {
            auto t = i / double(segments1);
            touchPoints.emplace_back(bezierFunctionX(t));
            touchPoints.emplace_back(bezierFunctionY(t));
            touchIndices.emplace_back(index);
            pointColors.insert(pointColors.end(), colorVector.begin(), colorVector.end());
            numPoints = index + 1;
            index += 1;
        }
    }
}
*/

void initializeFrameBuffer(){
    // Create a framebuffer object (FBO)
    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

    // The texture we're going to render to
    glGenTextures(1, &renderedTexture);
    // "Bind" the newly created texture : all future texture functions will modify this texture
    glBindTexture(GL_TEXTURE_2D, renderedTexture);

    // Give an empty image to OpenGL
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, screen_width, screen_height, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    // attach it to currently bound framebuffer object
    // Set "renderedTexture" as our colour attachement #0
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, renderedTexture, 0);

    // Set the list of draw buffers.
    //GLenum DrawBuffers[1] = {GL_COLOR_ATTACHMENT0};
    //glDrawBuffers(1, DrawBuffers); // "1" is the size of DrawBuffers

    // Create a render buffer object (RBO)
    GLuint rbo;
    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, screen_width, screen_height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);

    // Always check that our framebuffer is ok
    auto fboStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (fboStatus != GL_FRAMEBUFFER_COMPLETE) {
        LOGE("Framebuffer not complete: %u", fboStatus);
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void renderFrame(float x, float y, int actionUp){ //onDrawFrame
    // Our ModelViewProjection : multiplication of our 3 matrices
    //mvp = Projection * View * Model; // Remember, matrix multiplication is the other way around

    // Send our transformation to the currently bound shader, in the "MVP" uniform
    // This is done in the main loop since each model will have a different MVP matrix (At least for the M part)
    //glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &mvp[0][0]);

    shaderProgram = createProgram(glVertexShader, glFragmentShader);

    /* Code for checking Alpha blending
    glClear (GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
    glUseProgram(shaderProgram);

    vPosition = glGetAttribLocation(shaderProgram, "vPosition");
    vColor = glGetAttribLocation(shaderProgram, "vColor");

    for (int i = 0; i < 2; i++){
        glVertexAttribPointer(vPosition, 2, GL_FLOAT, GL_FALSE, 0, vertices[i]);
        glVertexAttribPointer(vColor, 4, GL_FLOAT, GL_FALSE, 0, colors[i]);
        glEnableVertexAttribArray(vPosition);
        glEnableVertexAttribArray(vColor);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    }
    */

    ///------------- Touch points -------------///
    if (actionUp == 0) { //screen touch
        points.emplace_back(Point(x, y));

        // Bezier points - Method 1
        step += 1;
        if (step == -1) {
            touchPoints.emplace_back(x * 2.0f / float(screen_width) - 1.0f);
            touchPoints.emplace_back(y * -2.0f / float(screen_height) + 1.0f);
            touchIndices.emplace_back(index);
            pointColors.insert(pointColors.end(), colorVector.begin(), colorVector.end());
            numPoints = index + 1;
            index += 1;
        } else {
            bezierGeneration1(step);
        }
        //

        /* Bezier points - Method 2
        if (counter == 0) {
            touchPoints.emplace_back(x * 2.0f / float(screen_width) - 1.0f);
            touchPoints.emplace_back(y * -2.0f / float(screen_height) + 1.0f);
            touchIndices.emplace_back(index);
            pointColors.insert(pointColors.end(), colorVector.begin(), colorVector.end());
            numPoints = index + 1;
            index += 1;
        } else{
            bezierGeneration2(counter);
        }
        counter += 1;
        */

        // Make arrays using vectors
        GLfloat *touches = &touchPoints[0];
        GLint *indices = &touchIndices[0];
        GLfloat *colors = &pointColors[0];

        // Create and initialize a buffer object for each attribute
        GLuint buffer[2];
        glGenBuffers(2, buffer);

        // Bind it to GL_ARRAY_BUFFER and pass the data to the GPU
        glBindBuffer(GL_ARRAY_BUFFER, buffer[0]);
        glBufferData(GL_ARRAY_BUFFER, touchPoints.size() * sizeof(GLfloat), touches, GL_STATIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, buffer[1]);
        glBufferData(GL_ARRAY_BUFFER, pointColors.size() * sizeof(GLfloat), colors, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        // Create a buffer for the indices
        GLuint indexBuffer;
        glGenBuffers(1, &indexBuffer);

        // Bind it to GL_ARRAY_BUFFER and pass the data to the GPU
        glBindBuffer(GL_ARRAY_BUFFER, indexBuffer);
        glBufferData(GL_ARRAY_BUFFER, touchIndices.size() * sizeof(GLfloat), indices, GL_STATIC_DRAW);

        // Create a vertex array object (VAO)
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);

        // Initialize the vertex vPosition attribute defined in the vertex shader
        // Get an index for the attribute from the shader
        GLuint pos = glGetAttribLocation(shaderProgram, "vPosition");
        glEnableVertexAttribArray(pos);

        GLuint col = glGetAttribLocation(shaderProgram, "vColor");
        glEnableVertexAttribArray(col);

        // Associate the attribute with the data in the buffer.
        // glVertexAttribPointer implicitly refers to the currently bound GL_ARRAY_BUFFER
        glBindBuffer(GL_ARRAY_BUFFER, buffer[0]);
        glVertexAttribPointer(pos, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

        glBindBuffer(GL_ARRAY_BUFFER, buffer[1]);
        glVertexAttribPointer(col, 4, GL_FLOAT, GL_FALSE, 0, nullptr);

        // Add indices for indexed rendering
        // Binding to GL_ELEMENT_ARRAY_BUFFER is saved with VAO state
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);

        // Unbind the buffer
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        // Unbind the VAO
        glBindVertexArray(0);

        // bind the shaders
        glUseProgram(shaderProgram);

        glClearColor(1.0, 1.0, 1.0, 1.0);
        // clear the window
        glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

        // bind the VAO
        glBindVertexArray(vao);

        loadSimpleTexture(whichTexture);

        glDrawElements(GL_POINTS, numPoints, GL_UNSIGNED_INT, nullptr);
        //glFlush();

        // unbind
        glUseProgram(0);
        glBindVertexArray(0);

    } else if (actionUp == 1){
        step = -2;
        counter = 0;
        colorVector.clear();
        if (turn == 1){
            colorVector = {0.1, 0.9, 0.1, 1.0};
            turn = 2;
        }else if (turn == 2){
            colorVector = {0.1, 0.1, 0.9, 1.0};
            turn = 3;
        } else{
            colorVector = {0.9, 0.1, 0.1, 1.0};
            turn = 1;
        }
    }
    ///------------------------------------------///

    ///------------- Render targets -------------///
    /*
    if (needRenderTarget) {
        GLuint VAO1, VBO1[2];
        glGenVertexArrays(1, &VAO1);
        glBindVertexArray(VAO1);

        glGenBuffers(2, VBO1);
        glBindBuffer(GL_ARRAY_BUFFER, VBO1[0]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices[0]), &vertices[0], GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, VBO1[1]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(texCords[0]), &texCords[0], GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        GLuint pos1 = glGetAttribLocation(shaderProgram, "vPosition");
        glEnableVertexAttribArray(pos1);
        GLuint tex1 = glGetAttribLocation(shaderProgram, "vertexTextureCord");
        glEnableVertexAttribArray(tex1);

        glBindBuffer(GL_ARRAY_BUFFER, VBO1[0]);
        glVertexAttribPointer(pos1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
        glBindBuffer(GL_ARRAY_BUFFER, VBO1[1]);
        glVertexAttribPointer(tex1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);

        initializeFrameBuffer();

        glBindFramebuffer(GL_FRAMEBUFFER, fbo);
        glEnable(GL_DEPTH_TEST);

        glUseProgram(shaderProgram);
        glClearColor(0.1, 1.0, 0.1, 1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glBindVertexArray(VAO1);

        whichTexture = 1;
        loadSimpleTexture(whichTexture);

        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

        glBindVertexArray(0);

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glDisable(GL_DEPTH_TEST);
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        GLuint VAO2, VBO2[2];
        glGenVertexArrays(1, &VAO2);
        glBindVertexArray(VAO2);

        glGenBuffers(2, VBO2);
        glBindBuffer(GL_ARRAY_BUFFER, VBO2[0]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices[2]), &vertices[2], GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, VBO2[1]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(texCords[2]), &texCords[2], GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        GLuint pos2 = glGetAttribLocation(shaderProgram, "vPosition");
        glEnableVertexAttribArray(pos2);
        GLuint tex2 = glGetAttribLocation(shaderProgram, "vertexTextureCord");
        glEnableVertexAttribArray(tex2);

        glBindBuffer(GL_ARRAY_BUFFER, VBO2[0]);
        glVertexAttribPointer(pos2, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
        glBindBuffer(GL_ARRAY_BUFFER, VBO2[1]);
        glVertexAttribPointer(tex2, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

        glBindVertexArray(VAO2);
        glBindTexture(GL_TEXTURE_2D, renderedTexture);

        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

        needRenderTarget = false;
    }
    */
    ///------------------------------------------///

    /* Load texture
    int width, height, nrChannels;
    const char* filename = "chalk-1.png";
    unsigned char* data = stbi_load(filename, &width, &height, &nrChannels, 0);
    if (data == nullptr){
        LOGE("Failed to load texture");
    }
    LOGE("width = %d, height = %d, channels = %d", width, height, nrChannels);

    GLuint texture;
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glGenTextures(1, &texture);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glEnable(GL_TEXTURE_2D);

    stbi_image_free(data);
    */
}

extern "C" {
    void Java_com_example_testingbrush_CCERenderer_init(JNIEnv* env, jclass obj) {
        init();
    }

    void Java_com_example_testingbrush_CCERenderer_resize(JNIEnv* env, jclass obj, jint width, jint height) {
        resize(width, height);
    }

    void Java_com_example_testingbrush_CCERenderer_render(JNIEnv* env, jclass obj, jfloat x, jfloat y, jint isActionUp) {
        renderFrame(x, y, isActionUp);
    }
}