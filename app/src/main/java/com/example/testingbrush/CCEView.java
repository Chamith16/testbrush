package com.example.testingbrush;

import android.content.Context;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;

public class CCEView extends GLSurfaceView {
    private final CCERenderer cceRenderer;
    private VelocityTracker velocityTracker = null;
    int isActionUp = 1;

    public CCEView(Context context){
        super(context);
        cceRenderer = new CCERenderer(context);
        setEGLContextClientVersion(3);
        //getHolder().setFormat(PixelFormat.OPAQUE);
        setEGLConfigChooser(8,8,8,8,16,0);
        setRenderer(cceRenderer);
        setPreserveEGLContextOnPause(true);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e){
        int index = e.getActionIndex();
        //int pointerId = e.getPointerId(index);
        float x, y;

        switch (e.getAction()){
            case MotionEvent.ACTION_DOWN:{
                Log.e("CCEView", "ACTION DOWN");
                if (velocityTracker == null){
                    velocityTracker = VelocityTracker.obtain();
                } else {
                    velocityTracker.clear();
                }
                velocityTracker.addMovement(e);
                break;
            }

            case MotionEvent.ACTION_MOVE:{
                x = e.getX();
                y = e.getY();
                isActionUp = 0;
                cceRenderer.setCoords(x, y, isActionUp);
                velocityTracker.computeCurrentVelocity(1000);
                requestRender();
                break;
            }

            case MotionEvent.ACTION_UP:{
                Log.e("CCEView", "ACTION UP");
                isActionUp = 1;
                cceRenderer.setActionUp(isActionUp);
                break;
            }
        }
        return true;
    }
}
