package com.example.testingbrush;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

public class CCEActivity extends AppCompatActivity {
    public static CCEView glView;

    static {
        System.loadLibrary("native-lib");
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        //this.setContentView(R.layout.main_layout);
        //this.initView(findViewById(R.id.of_gl_surface_container));
        glView = new CCEView(this);
        setContentView(glView);
    }

    @Override
    protected void onPause(){
        super.onPause();
        glView.onPause();
    }

    @Override
    protected void onResume(){
        super.onResume();
        glView.onResume();
    }
}
