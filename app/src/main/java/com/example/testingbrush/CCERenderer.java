package com.example.testingbrush;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLES32;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.Log;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class CCERenderer implements GLSurfaceView.Renderer {
    float x, y;
    int actionUp = -1;
    boolean isTouch = false;

    public CCERenderer(Context context){}

    public void setCoords(float xPos, float yPos, int isActionUp){
        x = xPos;
        y = yPos;
        actionUp = isActionUp;
        isTouch = true;
    }

    public void setActionUp(int isActionUp){
        actionUp = isActionUp;
        isTouch = true;
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig eglConfig) {
        Log.e("CCERenderer", "---> onSurfaceCreated");
        init();

        GLES32.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        GLES32.glClearDepthf(1.0f);
        GLES32.glEnable(GLES20.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES20.GL_LEQUAL);
        GLES32.glDisable(GLES20.GL_DITHER);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        Log.e("CCERenderer", "---> onSurfaceChanged");
        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        //Log.e("CCERenderer", "---> onDrawFrame");
        if (!isTouch){
            actionUp = -1;
        }
        isTouch = false;
        render(x, y, actionUp);
    }

    public static native void init();
    public static native void resize(int width, int height);
    public static native void render(float x, float y, int actionUp);
}
